﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Helpers
{
    public static class FoundYearAgo
    {
        public static int GetYearAgo(this DateTime dateTime)
        {
            var currentDate = DateTime.Now;
            int yearAgo = currentDate.Year - dateTime.Year;

            return yearAgo;
        }
    }
}
