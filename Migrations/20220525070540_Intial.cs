﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BandAPI.Migrations
{
    public partial class Intial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bands",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Founded = table.Column<DateTime>(nullable: false),
                    MainGenre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 400, nullable: false),
                    BandId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Bands_BandId",
                        column: x => x.BandId,
                        principalTable: "Bands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[,]
                {
                    { new Guid("ac775b46-da7b-11ec-9d64-0242ac120002"), new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Heavy Metal", "Metallica" },
                    { new Guid("bc5a6d40-da7c-11ec-9d64-0242ac120002"), new DateTime(1985, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rock", "Guns N Roses" },
                    { new Guid("4faa06c8-da7d-11ec-9d64-0242ac120002"), new DateTime(1965, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Disco", "ABBA" },
                    { new Guid("56a0a144-da7d-11ec-9d64-0242ac120002"), new DateTime(1991, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alternative", "Oasis" },
                    { new Guid("5c5f4b94-da7d-11ec-9d64-0242ac120002"), new DateTime(1981, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pop", "A-ha" }
                });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "BandId", "Description", "Title" },
                values: new object[,]
                {
                    { new Guid("3f83fed8-da7e-11ec-9d64-0242ac120002"), new Guid("ac775b46-da7b-11ec-9d64-0242ac120002"), "One of the best heavy metal albums ever", "Master Of Puppets" },
                    { new Guid("c6365c96-da7e-11ec-9d64-0242ac120002"), new Guid("bc5a6d40-da7c-11ec-9d64-0242ac120002"), "Amazing Rock album with raw sound", "Appetile for Destruction" },
                    { new Guid("d1f016d0-da7e-11ec-9d64-0242ac120002"), new Guid("4faa06c8-da7d-11ec-9d64-0242ac120002"), "Very groovy album", "Waterloo" },
                    { new Guid("d7de690c-da7e-11ec-9d64-0242ac120002"), new Guid("56a0a144-da7d-11ec-9d64-0242ac120002"), "Arguably one of the best albums by Oasis", "Be Here Now" },
                    { new Guid("dde3ca22-da7e-11ec-9d64-0242ac120002"), new Guid("5c5f4b94-da7d-11ec-9d64-0242ac120002"), "Awesome Debut album by A-ha", "Hunting Hight and Low" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_BandId",
                table: "Albums",
                column: "BandId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Bands");
        }
    }
}
