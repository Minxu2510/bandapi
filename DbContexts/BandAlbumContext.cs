﻿using BandAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.DbContexts
{
    public class BandAlbumContext : DbContext
    {
        public BandAlbumContext(DbContextOptions<BandAlbumContext> options) : base (options)
        {
        }

        public DbSet<Band> Bands { get; set; }
        public DbSet<Album> Albums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Band>().HasData(new Band()
            {
                Id = Guid.Parse("ac775b46-da7b-11ec-9d64-0242ac120002"),
                Name = "Metallica",
                Founded = new DateTime(1980, 1, 1),
                MainGenre = "Heavy Metal"
            },
            new Band
            {
                Id = Guid.Parse("bc5a6d40-da7c-11ec-9d64-0242ac120002"),
                Name = "Guns N Roses",
                Founded = new DateTime(1985, 2, 1),
                MainGenre = "Rock"
            },
            new Band
            {
                Id = Guid.Parse("4faa06c8-da7d-11ec-9d64-0242ac120002"),
                Name = "ABBA",
                Founded = new DateTime(1965, 7, 1),
                MainGenre = "Disco"
            }, 
            new Band
            {
                Id = Guid.Parse("56a0a144-da7d-11ec-9d64-0242ac120002"),
                Name = "Oasis",
                Founded = new DateTime(1991, 12, 1),
                MainGenre = "Alternative"
            },
            new Band
            {
                Id = Guid.Parse("5c5f4b94-da7d-11ec-9d64-0242ac120002"),
                Name = "A-ha",
                Founded = new DateTime(1981, 6, 1),
                MainGenre = "Pop"
            });

            modelBuilder.Entity<Album>().HasData(
                new Album
                {
                    Id = Guid.Parse("3f83fed8-da7e-11ec-9d64-0242ac120002"),
                    Title = "Master Of Puppets",
                    Description = "One of the best heavy metal albums ever",
                    BandId = Guid.Parse("ac775b46-da7b-11ec-9d64-0242ac120002")
                },
                new Album
                {
                    Id = Guid.Parse("c6365c96-da7e-11ec-9d64-0242ac120002"),
                    Title = "Appetile for Destruction",
                    Description = "Amazing Rock album with raw sound",
                    BandId = Guid.Parse("bc5a6d40-da7c-11ec-9d64-0242ac120002")
                },
                new Album
                {
                    Id = Guid.Parse("d1f016d0-da7e-11ec-9d64-0242ac120002"),
                    Title = "Waterloo",
                    Description = "Very groovy album",
                    BandId = Guid.Parse("4faa06c8-da7d-11ec-9d64-0242ac120002")
                },
                new Album
                {
                    Id = Guid.Parse("d7de690c-da7e-11ec-9d64-0242ac120002"),
                    Title = "Be Here Now",
                    Description = "Arguably one of the best albums by Oasis",
                    BandId = Guid.Parse("56a0a144-da7d-11ec-9d64-0242ac120002")
                },
                new Album
                {
                    Id = Guid.Parse("dde3ca22-da7e-11ec-9d64-0242ac120002"),
                    Title = "Hunting Hight and Low",
                    Description = "Awesome Debut album by A-ha",
                    BandId = Guid.Parse("5c5f4b94-da7d-11ec-9d64-0242ac120002")
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
